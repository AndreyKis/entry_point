const express = require('express');
const externalProfile = require('../modules/external-profile/index');
const stripe = require('../modules/stripe-interaction/index')('sk_test_u695NpdETpOy0TkDhex7ujib');
const winstonLoggerWrapper = require('../modules/winston-logger-wrapper/index');

const app = express();

app.use('/external/test', (req, res, next) => {
    externalProfile.getProfileByToken("4765739641.1677ed0.542d7428ed5d4d39a7a6300d11443680dfdfdf", 'instagram')
        .then((profile) => {
            console.log(profile);
        })
        .catch((err) => {
            console.error(err);
        })
});

app.use('/winston/test', (req, res, next) => {
    winstonLoggerWrapper.errorLogger.error(new Error("Test error"));
    winstonLoggerWrapper.infoLogger.info(new Error("Test info"));
});

app.use('/stripe/test', (req, res, next) => {
    let createdStripeUserInfo;
    stripe.generateTwoTokens()
        .then((tokens) => {
            return stripe.addPaymentMethod(tokens);
        })
        .then((stripeUserInfo) => {
            createdStripeUserInfo = stripeUserInfo;
            return stripe.generateTwoTokens();
        })
        .then((tokens) => {
            return stripe.editCard({
                customerId: createdStripeUserInfo.createdCustomer.id,
                accountId: createdStripeUserInfo.createdAccount.id
            }, {
                customerCardTokenId: tokens.customerCardToken.id,
                accountCardTokenId: tokens.accountCardToken.id,
            });
        })
        .then((editedCard) => {
            return stripe.deleteCard({
                customerId: createdStripeUserInfo.createdCustomer.id,
                accountId: createdStripeUserInfo.createdAccount.id
            })
        })
        .then((editedCard) => {
            console.log("SUCCESS");
        })
        .catch((err) => {
            winstonLoggerWrapper.errorLogger.error(err);
        })
});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
